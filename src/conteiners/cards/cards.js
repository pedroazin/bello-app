import React from 'react'
import { connect } from 'react-redux'
import Card from '../../components/card/card'


const Cards = ({ cards, listId }) => {

  const renderCards = () => cards
    .filter(card => card.listId === listId)
    .map(card => <Card key={card.id} description={card.description} />)

  return (<div className="Cards">
    {renderCards()}
  </div>)
}

const mapStateToProps = ({ cards }) => ({ cards })

export default connect(mapStateToProps)(Cards);