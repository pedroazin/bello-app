import React from 'react'
import { connect } from 'react-redux'
import { showAddList, hideAddList } from '../../actions/visibilityAddList/visibilityAddList'
import { addList } from '../../actions/lists/lists'
import "./addList.css"

const AddList = ({ visibilityAddList, showAddList, hideAddList, addListe   }) => {

  let input;

  return (
    <div className="AddList">
      {
        visibilityAddList ?
          (
            <div className="AddList-add-show">
              <form className="AddList-form" onSubmit={(e) => addListe(e, input)}>
                <input className="AddList-list-title" ref={(node) => {input = node}} placeholder="Adicione uma lista..." />
                <div className="ListAdd-actions">
                  <button type="submit">Adicionar</button>
                  <button type="button" onClick={hideAddList} >X</button>
                </div>
              </form>
            </div>) :
          (<div className="AddList-add-hide">
            <span className="AddList-title" onClick={showAddList}> Adicionar uma lista ...</span>
          </div>)
      }
    </div >
  );
}

const mapStateToProps = ({ visibilityAddList }) => ({ visibilityAddList })
const mapDispatchToProps = (dispatch) => ({
  showAddList: () => dispatch(showAddList()),
  hideAddList: (e) => {
    e.preventDefault()
    dispatch(hideAddList())
  },
  addListe: (e, input)=>{
    e.preventDefault()
    dispatch(addList(input.value))
    input.value =""
    input.focus()
  }
})


export default connect(mapStateToProps, mapDispatchToProps)(AddList);