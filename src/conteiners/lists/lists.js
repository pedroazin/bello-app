import React, { Component } from 'react'
import { connect } from 'react-redux'
import AddList from '../addList/addList'
import { scroolEndPageX } from '../../utils'
import List from '../../components/list/list'
import "./lists.css"


class Lists extends Component {

  componentDidUpdate(){
    console.log("Chamnou")
    scroolEndPageX('#Lists')
  }

  render() {
    const { lists } = this.props

    const renderList = () => lists.map(list => <List key={list.id} listId={list.id} title={list.title} />)

    return (
      <div id="Lists" className="Lists">
        {renderList()}
        <AddList />
      </div>)
  }
}

const mapStateToProps = ({ lists }) => ({ lists })

export default connect(mapStateToProps)(Lists);