import React from 'react'
import Lists from '../lists/lists'
import { connect } from 'react-redux'

import "./board.css"

const board = ({ board }) => {
  return (
    <div>
      <div className="Board-header">
        <span className="Board-project">{board.project}</span>
      </div>
      <div className="Board-body">
        <Lists />
      </div>
    </div>
  )
}


const mapStateToProps = ({ board }) => ({ board })

export default connect(mapStateToProps)(board);
