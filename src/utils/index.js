const scroolEndPageX = (selector) => {
  const element = document.querySelector(selector)
  const width = element.scrollWidth
  element.scrollTo(width,0)
}

export {
  scroolEndPageX
}