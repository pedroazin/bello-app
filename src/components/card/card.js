import React from 'react'
import "./card.css"

export default ({ description }) => {
  return (
    <div className="Card">
      <div>
        <div className="Card-header">{description}</div>
      </div>
    </div>
  );
}
