import React from 'react'
import "./header.css"

const Header = (props) => {
  return (
    <header className="Header">
      <span className="Header-logo">Bello</span>
    </header>
  );
}

export default Header;