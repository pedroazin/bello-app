import React from 'react'
import './list.css'

import Cards from '../../conteiners/cards/cards'

const List = ({ title, listId }) => (
  <div className="List">
    <div className="List-header">
      <span className='List-header-title'>{title}</span>
    </div>
    <div className="List-body">
      <Cards listId={listId} />
    </div>
    <div className="List-footer">
      <button className="List-footer-btn-show-add-card" >Adicionar um cartão...</button>
    </div>
  </div>
);

export default List;