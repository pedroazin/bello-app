import {
  combineReducers
} from 'redux'
import {
  createStore
} from 'redux'
import { board, lists ,cards , visibilityAddList} from './reducers'


const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()

const reducers = combineReducers({
  board,
  lists,
  cards,
  visibilityAddList
})

export default createStore(reducers, devTools)