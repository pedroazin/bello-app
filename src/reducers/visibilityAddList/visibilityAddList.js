import * as Types from '../../types'

const initialState = false;

export default (state = initialState, { type }) => {

  switch (type) {
    case Types.visibilityAddList.SHOW_ADD_LIST:
      return true
    case Types.visibilityAddList.HIDE_ADD_LIST:
      return false
    default:
      return state
  }
}