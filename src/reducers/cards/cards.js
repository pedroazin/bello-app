const initialState = [
  {
    id: 1,
    description: 'Comprar Pao',
    listId: 1
  },
  {
    id: 2,
    description: 'Comprar Arroz',
    listId: 2
  },
  {
    id: 3,
    description: 'Ler um livro',
    listId: 3
  }
]

export default (state = initialState, { type, payload }) => {
  return state
}