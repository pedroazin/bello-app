import * as Types from '../../types'

const initialState = [
  {
    id: 1,
    title: 'A Fazer'
  },
  {
    id: 2,
    title: 'Fazendo'
  },
  {
    id: 3,
    title: 'Feito'
  }

]

export default (state = initialState, { type, payload }) => {

  switch (type) {
    case Types.lists.ADD_LIST:
      return [
        ...state,
        { id: state.length + 1, title: payload }
      ]
    default:
      return state
  }


}