import * as Types from '../../types'

const showAddList = () => ({ type: Types.visibilityAddList.SHOW_ADD_LIST })
const hideAddList = () => ({ type: Types.visibilityAddList.HIDE_ADD_LIST })


export { showAddList, hideAddList }