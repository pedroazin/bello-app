import * as Types from '../../types'

const addList = (name) => ({
  type: Types.lists.ADD_LIST,
  payload: name
})


export { 
  addList
}