import React, { Component } from 'react';
import Header from './components/header/header'
import Board from './conteiners/board/board'
import './App.css';

class App extends Component {
  
  render() {
    return (
      <div className="App" style={{ background: '#0079bf' }}>
        <Header />
        <Board />
      </div>
    );
  }
}

export default App;
